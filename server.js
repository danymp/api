//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson = require('request-json');
var urlMBLABRaiz = "https://api.mlab.com/api/1/databases/jmontes/collections";
var apikey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteurlMBLABRaiz;
var urlClienteMongo = "https://api.mlab.com/api/1/databases/jmontes/collections/clientes?";


var clienteMlab = requestjson.createClient(urlClienteMongo);
var bodyParse = require('body-parser');
app.use(bodyParse.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  res.sendfile('index.html');
});

app.post("/",function(req,res){
  res.send("Hemos recibido su peticion POST");
});

app.put("/",function(req,res){
  res.send("Hemos recibido su peticion PUT");
});

app.delete("/",function(req,res){
  res.send("Hemos recibido su peticion DELETE");
});


/// CLIENTES
app.get("/customers",function(req,res){
  res.send("Hemos recibido su peticion GET de clientes");
});

app.get("/customers/:idcliente",function(req,res){
  res.send("Aqui tiene el cliente: " + req.params.idcliente);
});


app.get("/v1/movimientos",function(req,res){
  res.sendfile('movimientosv1.json');
});

// VERSION CAMBIADA

var movimientosv2 = require('./movimientosv2.json');

app.get("/v2/movimientos",function(req,res){
  res.json(movimientosv2);
});


app.get("/v2/movimientos/:id",function(req,res){
  console.log(req.params.id);
  res.json(movimientosv2[req.params.id]);
});

// QUERY
app.get("/v2/movimientosq",function(req,res){
  console.log(req.query);
  //res.send("Peticion con Query recivida: " + JSON.stringify(req.query));
  res.json(movimientosv2[req.query.id]);
});


app.get("/v2/cliente",function(req,res){
  console.log(req.query);
  clienteMlab.get('',function(err,resM,body){
    if (err) {
      console.log(body);
    }else{
      res.send(body);
    }
  });
});


app.post("/v2/cliente",function(req,res){
  clienteMlab.post('',req.body,function(err,resM,body){
    if (err) {
      console.log(body);
    }else{
      res.send(body);
    }
  });
});

app.post('/v2/login',function(req,res){
  console.log("INICIANDO LOGIN");
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  var email = req.headers.email;
  var password = req.headers.password;
  var query = 'q={"email":"'+email+'", "password":"'+password+'"}';
  //console.log("QUERY CONSULTA: " + query);
  clienteurlMBLABRaiz = requestjson.createClient(urlMBLABRaiz + "/usuarios?" + apikey + "&" + query);

  console.log("######### URL CONSULTA BASE DE DATOS   #########");
  console.log(clienteurlMBLABRaiz);
  console.log("######### ######### #########");


  clienteMlab = clienteurlMBLABRaiz.get('',function(err,resM,body){
    if(!err){
      console.log("#########");
      console.log(body.length);
      console.log("#########");
      if(body.length > 0){
        res.status(200).send("Usuario logueado correctamente");
      }else{
        res.status(403).send("Usuario error");
      }
    }else{
      console.log("ALGO FALLO");
      res.status(500).send("Ocurrio un error");
    }
  });
});
